/*
G_MAIN
The part of the code that actually has the game code, and is safe to modify.
*/

#include "g_main.h"
#include "g_draw.h"

#include <windows.h>

#define WINDOW_WIDTH 960
#define WINDOW_HEIGHT 540

const int MS = 17;
// DOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOMMMmmmmmmmm

// this code over here is needed for the Windows API part of the pipeline to do things for certain events

void WinPipe_WMCreate() { // WM_Create event, it tells the program what to do when the program starts
	return;
};

void WinPipe_WMTimer() { // WM_Timer event, basically every frame
	return;
};

void WinPipe_WMPaint() { // WM_Paint event, it tells the program what to draw to the window
	return;
};