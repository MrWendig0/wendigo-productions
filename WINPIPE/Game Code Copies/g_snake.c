/*
G_MAIN
The part of the code that actually has the game code, and is safe to modify.
*/

#include "g_main.h"
#include "g_draw.h"

#include <windows.h>
#include <time.h>
#include <stdlib.h>
#include <mmsystem.h>
#pragma comment(lib, "Winmm")

#define GAMENAME "Snake: WinPipe Port"
#define WINDOW_WIDTH 400
#define WINDOW_HEIGHT 400

int snake_hdir = 0;
int snake_vdir = 0;
int snake_blocks[100][2];
int MAXSNAKEBLOCKS = 100;
int snake_length = 2;
int food_x = 1;
int food_y = 1;

const int MS = 90;
/*
the Snake game runs with 90 MS per frame, a bit short of 11 FPS, feel free to change this to a float if you want
I'll provide a little lookup table down here in case your game runs at a different framerate:

60 FPS:
16.667 as float
17 as integer

30 FPS:
33.333 as float
33 as integer

24 FPS:
41.667 as float
42 as integer
*/

void changeSnakeBlocks(int input_1, int input_2) {
	snake_blocks[input_1][0] = snake_blocks[input_2][0];
	snake_blocks[input_1][1] = snake_blocks[input_2][1];
}

void MoveSnake() {
	if (snake_hdir + snake_vdir == 0) return;
	// move the closest block to the head's position...
	changeSnakeBlocks(1, 0);
	// ...THEN move the head
	snake_blocks[0][0] += snake_hdir;
	snake_blocks[0][1] += snake_vdir;
	for (int i = snake_length; i > 1; i--) {
		if (snake_blocks[i - 1][0] == -1) continue; // no block here? skip to the next one
		changeSnakeBlocks(i, i -1);
	}
	//PlaySound(TEXT("data/snake_move.wav"), NULL, SND_FILENAME | SND_ASYNC);
};

void RegenFood() {
	food_x = rand() % 48;
	food_y = rand() % 48;
	return;
};

// this code over here is needed for the Windows API part of the pipeline to do things for certain events

void WinPipe_WMCreate() { // WM_Create event
	for (int i = 0; i < MAXSNAKEBLOCKS - 1; i++) {
		snake_blocks[i][0] = -1;
	};
	snake_blocks[0][0] = snake_blocks[0][1] = 24;
	snake_blocks[1][0] = 24;
	snake_blocks[1][1] = 25;
	RegenFood();
	//LPHMIXER mixer1;
	//mixerOpen(&mixer1, 1, NULL, 0, MIXER_OBJECTF_HMIXER);
};

void WinPipe_WMTimer() { // WM_Timer event, basically per time the timer's ran
	MoveSnake();
	if (snake_blocks[0][0] == food_x && snake_blocks[0][1] == food_y) {
		snake_length++;
		changeSnakeBlocks(snake_length, snake_length - 1);
		RegenFood();
		PlaySound(TEXT("data/snake_getfood.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_NOSTOP);
	}
	for (int i = MAXSNAKEBLOCKS; i > 1; i--) {
		if (snake_blocks[0][0] == snake_blocks[i][0] && snake_blocks[0][1] == snake_blocks[i][1]) {
			for (int i = 0; i < MAXSNAKEBLOCKS - 1; i++) {
				snake_blocks[i][0] = -1;
			};
			snake_length = 2;
			snake_blocks[0][0] = snake_blocks[0][1] = 24;
			snake_blocks[1][0] = 24;
			snake_blocks[1][1] = 25;
			snake_hdir = 0;
			snake_vdir = 0;
			RegenFood();
		};
	};
};

void WinPipe_WMPaint() {
	/*
	
	SelectObject(hdc, GetStockObject(BLACK_PEN)); // white pen, because rectangles have outlines for some unorthrodox reason
	
	GetClientRect(hwnd, &rc);
	SelectObject(hdc, GetStockObject(BLACK_BRUSH)); // change brush to black
	Rectangle(hdc, -1, -1, 400, 400);
	#ifdef DEBUG
	//SetTextColor(hdc, RGB(255, 255, 255));
	//DrawText(hdc, "Debug mode enabled!", -1, %rc, DT_SINGLELINE | | DT_CENTER | DT_TOP);
	#endif
	
	for (int i = 0; i < 99; i++) {
		if (snake_blocks[i][0] == -1) continue;
		if (i == 0) SelectObject(hdc, GreenBrush); // color head bright green
		else SelectObject(hdc, DGreenBrush); // color body dark green
		Rectangle(hdc, snake_blocks[i][0] << 3, snake_blocks[i][1] << 3, snake_blocks[i][0] + 1 << 3, snake_blocks[i][1] + 1 << 3);
	}
	
	SelectObject(hdc, BlueBrush);
	Rectangle(hdc, food_x << 3, food_y << 3, food_x + 1 << 3, food_y + 1 << 3);
	*/
	ChangeBrush(4);
	DrawRectangle_Coords(-1, -1, 400, 400);
	for (int i = 0; i < 99; i++) {
		if (snake_blocks[i][0] == -1) continue;
		if (i == 0) ChangeBrush(2); // color head bright green
		else ChangeBrush(3); // color body dark green
		DrawRectangle_Coords(snake_blocks[i][0] << 3, snake_blocks[i][1] << 3, snake_blocks[i][0] + 1 << 3, snake_blocks[i][1] + 1 << 3);
	}
	ChangeBrush(0);
	DrawRectangle_Coords(food_x << 3, food_y << 3, food_x + 1 << 3, food_y + 1 << 3);
};

void WinPipe_WMKeyDown(WPARAM input) {
	switch (input)
	{
		//case 0x1B: // VK_ESCAPE
			//DestroyWindow(hwnd);
			//break;
		case 0x25: //VK_LEFTARROW
			snake_hdir = -1;
			snake_vdir = 0;
			break;
		case 0x27: //VK_RIGHTARROW
			snake_hdir = 1;
			snake_vdir = 0;
			break;
		case 0x26: //VK_UPARROW
			snake_vdir = -1;
			snake_hdir = 0;
			break;
		case 0x28: //VK_DOWNARROW
			snake_vdir = 1;
			snake_hdir = 0;
			break;
	}
};