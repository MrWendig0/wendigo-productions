#ifndef GAME
#define GAME

#include <windows.h>

int snake_hdir;
int snake_vdir;
int snake_blocks[100][2];
int MAXSNAKEBLOCKS;
int snake_length;
int food_x;
int food_y;

const int MS;

void changeSnakeBlocks(int input_1, int input_2);

void MoveSnake();

void RegenFood();

// this code over here is needed for the Windows API part of the pipeline to do things for certain events
void WinPipe_WMCreate();
void WinPipe_WMTimer();
void WinPipe_WMPaint();
void WinPipe_WMKeyDown(WPARAM input);

// TO-DO: in the Windows part of the code, make a system that allows things like drawing rectangles, setting colors, etc. from the game code

#endif