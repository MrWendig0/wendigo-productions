/*
G_MAIN
The part of the code that actually has the game code, and is safe to modify.
*/

#include "g_main.h"
#include "g_draw.h"

#include <windows.h>

#define WINDOW_WIDTH 400
#define WINDOW_HEIGHT 400

int border_x = 300;
int border_y = 300;
int cube_pos[2] = {150, 150};
int cube_size = 32; // size in all directions
int cube_dir[2] = {2, 1};

const int MS = 17;
const int WinPipe_WindowSize[2] = {316, 338};
/*
this test program runs at roughly 60 FPS
If you wanna change the FPS for your own game, I put some common framerates here:

60 FPS:
16.667 as float
17 as integer

30 FPS:
33.333 as float
33 as integer

24 FPS:
41.667 as float
42 as integer
*/

// this code over here is needed for the Windows API part of the pipeline to do things for certain events

void WinPipe_WMCreate() { // WM_Create event, it tells the program what to do when the program starts
	return;
};

void WinPipe_WMTimer() { // WM_Timer event, basically every frame
	cube_pos[0] += cube_dir[0];
	cube_pos[1] += cube_dir[1];
	if (cube_pos[0] - cube_size < 0 || cube_pos[0] + cube_size > border_x) {
		cube_pos[0] -= cube_dir[0];
		cube_dir[0] = -cube_dir[0];
	}
	if (cube_pos[1] - cube_size < 0 || cube_pos[1] + cube_size > border_y) {
		cube_pos[1] -= cube_dir[1];
		cube_dir[1] = -cube_dir[1];
	}
};

void WinPipe_WMPaint() { // WM_Paint event, it tells the program what to draw to the window
	ChangeBrush(3);
	DrawRectangle_Coords(-1, -1, border_x + 1, border_y + 1);
	ChangeBrush(2);
	DrawRectangle_Coords(
		cube_pos[0] - cube_size,
		cube_pos[1] - cube_size,
		cube_pos[0] + cube_size,
		cube_pos[1] + cube_size
	);
};