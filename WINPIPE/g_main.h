#ifndef GAME
#define GAME

#include <windows.h>

#define GAMENAME "3D Rendering: It's Hell on Earth"
#define WINDOW_WIDTH 960
#define WINDOW_HEIGHT 540

const int MS;

// this code over here is needed for the Windows API part of the pipeline to do things for certain events
void WinPipe_WMCreate();
void WinPipe_WMTimer();
void WinPipe_WMPaint();
void WinPipe_WMKeyDown(WPARAM input);

#endif