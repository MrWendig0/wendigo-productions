#include "g_draw.h"
#include "w_draw.h"

#include <windows.h>

/*
G_DRAW
Defines functions used for telling the Windows part of the code to do drawing.
If you don't know what you're doing, DON'T TOUCH THIS!
*/

// ChangeBrush
// Changes the current brush to a stock object. If under 0, it uses WinPipe's custom brushes.
void ChangeBrush(int brushindex, int custom) {
	WinPipe_DrawCommands[WinPipe_DrawCount][0] = 0x00000001;
	WinPipe_DrawCommands[WinPipe_DrawCount][1] = brushindex;
	WinPipe_DrawCommands[WinPipe_DrawCount][2] = custom;
	WinPipe_DrawCount++;
	return;
};
// ChangePen
// Changes the current pen to a stock object. If under 0, it uses WinPipe's custom pens
void ChangePen(int penindex, int custom) {
	WinPipe_DrawCommands[WinPipe_DrawCount][0] = 0x00000002;
	WinPipe_DrawCommands[WinPipe_DrawCount][1] = penindex;
	WinPipe_DrawCommands[WinPipe_DrawCount][2] = custom;
	WinPipe_DrawCount++;
	return;
};

void DrawRectangle(int x, int y, int w, int h, int align) {
	WinPipe_DrawCommands[WinPipe_DrawCount][0] = 0x00000003;
	WinPipe_DrawCommands[WinPipe_DrawCount][1] = x;
	WinPipe_DrawCommands[WinPipe_DrawCount][2] = y;
	WinPipe_DrawCommands[WinPipe_DrawCount][3] = w;
	WinPipe_DrawCommands[WinPipe_DrawCount][4] = h;
	WinPipe_DrawCommands[WinPipe_DrawCount][5] = align;
	WinPipe_DrawCount++;
};
void DrawRectangle_Coords(int x, int y, int x2, int y2) {
	WinPipe_DrawCommands[WinPipe_DrawCount][0] = 0x00000004;
	WinPipe_DrawCommands[WinPipe_DrawCount][1] = x;
	WinPipe_DrawCommands[WinPipe_DrawCount][2] = y;
	WinPipe_DrawCommands[WinPipe_DrawCount][3] = x2;
	WinPipe_DrawCommands[WinPipe_DrawCount][4] = y2;
	WinPipe_DrawCount++;
};
void DrawPixel(int x, int y, int R, int G, int B) {
	WinPipe_DrawCommands[WinPipe_DrawCount][0] = 0x00000005;
	WinPipe_DrawCommands[WinPipe_DrawCount][1] = x;
	WinPipe_DrawCommands[WinPipe_DrawCount][2] = y;
	WinPipe_DrawCommands[WinPipe_DrawCount][3] = R;
	WinPipe_DrawCommands[WinPipe_DrawCount][4] = G;
	WinPipe_DrawCommands[WinPipe_DrawCount][5] = B;
	WinPipe_DrawCount++;
};