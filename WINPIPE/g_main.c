/*
G_MAIN
The part of the code that actually has the game code, and is safe to modify.
*/

#include "g_main.h"
#include "g_draw.h"

#include <windows.h>

#define WINDOW_WIDTH 960
#define WINDOW_HEIGHT 540

const int MS = 17;
// DOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOMMMmmmmmmmm

void drawVertLine(int x, int height) {
	for (int i = height; i > height * -1; i -= 1) {
		DrawPixel(x, i, 0, 0, 0);
	}
}

// this code over here is needed for the Windows API part of the pipeline to do things for certain events

float p_x = 0.5;
float p_y = 50;
float p_z = 0;

float wall_x = -1;
float wall_y = 1;
float wall_h = 200;

void WinPipe_WMCreate() { // WM_Create event, it tells the program what to do when the program starts
	return;
};

void WinPipe_WMTimer() { // WM_Timer event, basically every frame
	return;
};

void WinPipe_WMPaint() { // WM_Paint event, it tells the program what to draw to the window
	ChangeBrush(1, 0);
	DrawRectangle_Coords(-1, -1, 960, 540);
	ChangePen(0, 0);
	drawVertLine((int)(wall_x / p_x + p_y), (int)wall_h);
	drawVertLine((int)(wall_y / p_x + p_y), (int)wall_h);
	
	ChangeBrush(2, 0);
	DrawRectangle_Coords(
		(int)(wall_x - (p_x + p_y)),
		(int)wall_h,
		(int)(wall_y - (p_x + p_y)),
		(int)wall_h
	);
};

void WinPipe_WMKeyDown(WPARAM input) {
	switch (input)
	{
		case 0x25: //VK_LEFTARROW
			p_x -= 1;
			break;
		case 0x27: //VK_RIGHTARROW
			p_x += 1;
			break;
		case 0x26: //VK_UPARROW
			p_y -= 1;
			break;
		case 0x28: //VK_DOWNARROW
			p_y += 1;
			break;
	}
};