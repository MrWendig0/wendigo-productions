// huge thanks to TinyCC's Windows API Hello World example program

#include <windows.h>

#include "w_draw.h"
#include "g_main.h"
#include "g_draw.h"

char szAppName[] = GAMENAME;
char szTitle[]   = GAMENAME;

void CenterWindow(HWND hWnd);
void ParseDrawCommands(HDC _hdc);

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message) {

        // ----------------------- first and last
        case WM_CREATE:
			WinPipe_WMCreate();
            CenterWindow(hwnd);
            SetTimer(hwnd, 1, MS, NULL); // "initialize" the "step"
            break;

        case WM_TIMER: // basically a step event
            InvalidateRect(hwnd, NULL, FALSE);
			WinPipe_WMTimer();
			WinPipe_WMPaint();
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        // ----------------------- get out of it...
        case WM_RBUTTONUP:
            DestroyWindow(hwnd);
            break;

		
        case WM_KEYDOWN: // input handling
			WinPipe_WMKeyDown(wParam);
            break;

        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC         hdc;
            hdc = BeginPaint(hwnd, &ps);
            SetBkMode(hdc, TRANSPARENT);
			
			ParseDrawCommands(hdc);
            EndPaint(hwnd, &ps);
            break;
        }

        // ----------------------- let windows do all other stuff
        default:
            return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

int APIENTRY WinMain(
        HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpCmdLine,
        int nCmdShow
        )
{
    MSG msg;
    WNDCLASS wc;
    HWND hwnd;

    // Fill in window class structure with parameters that describe
    // the main window.

    ZeroMemory(&wc, sizeof wc);
    wc.hInstance     = hInstance;
    wc.lpszClassName = szAppName;
    wc.lpfnWndProc   = (WNDPROC)WndProc;
    wc.style         = CS_DBLCLKS|CS_VREDRAW|CS_HREDRAW;
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);

    if (FALSE == RegisterClass(&wc))
        return 0;

    // create the window
    hwnd = CreateWindow(
        szAppName,
        szTitle,
        WS_OVERLAPPEDWINDOW|WS_VISIBLE,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        WINDOW_WIDTH,//CW_USEDEFAULT,
        WINDOW_HEIGHT,//CW_USEDEFAULT,
        0,
        0,
        hInstance,
        0
	);

    if (NULL == hwnd)
        return 0;

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0) > 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

void CenterWindow(HWND hwnd_self)
{
    HWND hwnd_parent;
    RECT rw_self, rc_parent, rw_parent;

    hwnd_parent = GetParent(hwnd_self);
    if (NULL == hwnd_parent)
        hwnd_parent = GetDesktopWindow();

    GetWindowRect(hwnd_parent, &rw_parent);
    GetClientRect(hwnd_parent, &rc_parent);
    GetWindowRect(hwnd_self, &rw_self);
	
    SetWindowPos(
        hwnd_self, NULL,
        rw_parent.left + (rc_parent.right + rw_self.left - rw_self.right) >> 1, //x pos
		rw_parent.top + (rc_parent.bottom + rw_self.top - rw_self.bottom) >> 1, //y pos
		0, 0,
        SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE
        );
}

// anyway here's an entire part of the code dedicated to parsing draw commands sent from the game
void ParseDrawCommands(HDC _hdc) {
	for (int i = 0; i < 65535; i++) {
		switch (WinPipe_DrawCommands[i][0])
		{
			case 0x00000001: // Change Brush
				if (WinPipe_DrawCommands[i][1] < 0) {
					if (WinPipe_DrawCommands[i][2] >= 1)
						SelectObject(_hdc, WinPipe_CustomBrushes[WinPipe_DrawCommands[i][1]]);
					else
						SelectObject(_hdc, GetStockObject(WinPipe_DrawCommands[i][1]));
					break;
				};
				break;
			case 0x00000002: // Change Pen
			// COPY PASTE THIS TO CHANGE BRUSH PLS KTHXBAI
				if (WinPipe_DrawCommands[i][1] < 0) {
					if (WinPipe_DrawCommands[i][2] >= 1)
						SelectObject(_hdc, WinPipe_CustomPens[WinPipe_DrawCommands[i][1]]);
					else
						SelectObject(_hdc, GetStockObject(WinPipe_DrawCommands[i][1]));
					break;
				};
				break;
			case 0x00000003: // Draw Rectangle
				Rectangle(_hdc, WinPipe_DrawCommands[i][1], WinPipe_DrawCommands[i][2], WinPipe_DrawCommands[i][3], WinPipe_DrawCommands[i][4]);
				break;
			case 0x00000004: // Draw Rectangle with Coordinates
				Rectangle(_hdc, WinPipe_DrawCommands[i][1], WinPipe_DrawCommands[i][2], WinPipe_DrawCommands[i][3], WinPipe_DrawCommands[i][4]);
				break;
			case 0x00000005: // Draw Pixel
				SetPixel(_hdc, WinPipe_DrawCommands[i][1], WinPipe_DrawCommands[i][2], RGB(WinPipe_DrawCommands[i][3], WinPipe_DrawCommands[i][4], WinPipe_DrawCommands[i][5]));
				break;
		}
		for (int ii = 0; ii < 8; ii++) {
			WinPipe_DrawCommands[i][ii] = 0;
		}
	}
	WinPipe_DrawCount = 0;
};

//+---------------------------------------------------------------------------
