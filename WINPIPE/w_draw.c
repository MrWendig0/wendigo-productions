/*
W_DRAW
Used to define things that need to be known by G_DRAW beforehand.
*/
#include "w_draw.h"

#include <windows.h>

int WinPipe_DrawCommands[65535][9];
int WinPipe_DrawCount = 0;
HPEN WinPipe_CustomPens[256];
HBRUSH WinPipe_CustomBrushes[256];
// 65535 commands, 2nd dimension can fit 1 entry for the type and 8 more for extra parameters
// DrawCount is there to make it easier to track the actual amount of commands needing parsing

#define	WPDRAW_CHANGEBRUSH 	0x00000001
#define	WPDRAW_CHANGEPEN	0x00000002
#define	WPDRAW_RECTANGLE	0x00000003
#define	WPDRAW_PIXEL 		0x00000004