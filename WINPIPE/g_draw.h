#ifndef GAME_DRAW
#define GAME_DRAW

/*
G_DRAW
Defines functions used for telling the Windows part of the code to do drawing.
*/

void ChangeBrush(int brushindex, int custom);
void ChangeCustomBrushColor(int R, int G, int B);
void ChangePen(int penindex, int custom);
void ChangeCustomPenColor(int R, int G, int B);

void DrawRectangle(int x, int y, int w, int h, int align);
void DrawRectangle_Coords(int x, int y, int x2, int y2);
void DrawPixel(int x, int y, int R, int G, int B);

#endif