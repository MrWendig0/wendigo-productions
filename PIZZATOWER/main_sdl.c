#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!
Mix_Chunk *music;

SDL_Surface *winsurf;

SDL_Surface *sprites[2];

SDL_Rect dsp_rect; // this is used for the sprite's boundaries
SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position

void drawSprite(SDL_Surface *sprite, int x, int y) {
	if (x == y) dspdest_rect.x = dspdest_rect.y = x;
	else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.w = sprite->w;
	dsp_rect.h = sprite->h;
	SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
};

void drawSpriteSheeted(SDL_Surface *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist) {
	if (x == y) dspdest_rect.x = dspdest_rect.y = x;
	else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.x = x_sprdist * frame;
	dsp_rect.w = x_sprdist;
	dsp_rect.h = y_sprdist;
	SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
};


void draw() {
	switch (GAME_STATE)
	{
		case 0:
			drawSprite(sprites[0], 0, 297);
			drawSprite(sprites[1], 293, 270 - (sprites[1]->h >> 1));
			break;
	};
};

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0) return 2;
	
	SDL_Window *win = SDL_CreateWindow("Pizza Tower: Sage 2019 Demo SDL port", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 960, 540, SDL_WINDOW_OPENGL);
	SDL_Renderer *render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
	
	winsurf = SDL_GetWindowSurface(win);
	
	music = Mix_LoadWAV("mu_title.wav");
	Mix_PlayChannel(-1, music, -1);
	
	sprites[0] = IMG_Load("sprites/menubg.png");
	sprites[1] = IMG_Load("sprites/title.png");
	
	uint8_t running = 1;
	SDL_Event event;
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		step();
		draw();
		
		SDL_UpdateWindowSurface(win);
		SDL_Delay(16.667f);
	}
	
	//for (int i = 0; i < 1; i++) {
	//	Mix_FreeMusic(music[i]);
	//}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};