/*
GAME.H
This file defines SPECIFICALLY the game logic.
Drawing and handling of sprites happens in the main_[platform].c file.
*/

// these definitions define the amount of frames in animations
#define PLR_IDLEFRAMES 7

#define SUBPIXELUNIT_ACCURACY 32 // according to Suckless, floats are overrated lmao
// floats can still be used... I think, it just needs code adaptation

#define STATE_NORMAL 0
#define STATE_MACH1 1
#define STATE_MACH2 2
#define STATE_MACH3 3
#define STATE_SUPERJUMP 4
#define STATE_GROUNDSLAM 5
#define STATE_GRAB 6
#define STATE_HAULING 7

#define FLAG_GROUNDED 1
#define FLAG_CANTMOVE 1 << 1 // player flag. if NOT set, player can control their character
#define FLAG_DOPHYS 1 << 2 // flag is universal. if NOT set, the object refuses to do gravity and speed additions, aka it won't move

#define GRAVITY SUBPIXELUNIT_ACCURACY / 4 // the precalculated gravity value for all objects

#define BADDIE_CHEESE 0
#define BADDIE_FORK 1
#define BADDIE_GOBLIN 2
#define BADDIE_BOMBER 3
#define BADDIE_WIZARD 4

#define GSTATE_MENU 0
#define GSTATE_PLAYING 1

uint8_t GAME_STATE = 0;

typedef struct {
	int x, y, hsp, vsp;
	int x_sub, y_sub, hsp_sub, vsp_sub;
	uint8_t state, flags;
} PT_Player;

typedef struct {
	uint16_t type;
	int x, y, hsp, vsp;
	int x_sub, y_sub, hsp_sub, vsp_sub;
	uint8_t state;
} PT_Baddie;

typedef struct {
	uint8_t menuselect;
} PT_MainMenu;

typedef struct {
	int x, y;
	uint16_t color;
} PT_Afterimage;

PT_Player plr; // the one and only Peppino Spaghetti

PT_Baddie *baddies[256]; // a pointer list of all baddies in the room
uint8_t baddiecount; // counts the baddies, for convenience
// this should clear out when a new room is about to be loaded

uint8_t level[4][256]; // world / level
// 4th world is for specifically custom content

PT_MainMenu menu; // main menu

void addBaddie(uint16_t inputtype) {
	if (baddiecount >= 255) return;
	PT_Baddie tempbad;
	tempbad.type = inputtype;
	baddies[baddiecount] = &tempbad;
	baddiecount++;
};

void step() {
	switch (plr.state)
	{
		case 0: break;
		case 1: break;
		case 2: break;
		case 3: break;
		case 4: break;
		case 5: break;
		case 6: break;
		case 7: break;
	}
	
	if (plr.flags & FLAG_DOPHYS) {
		if (!plr.flags & FLAG_GROUNDED) {
			plr.vsp_sub += GRAVITY;
			if (plr.vsp_sub > SUBPIXELUNIT_ACCURACY) plr.vsp += 1; plr.vsp_sub = 0;
		}
	};
	
	// to-do: iterate through all baddies currently in list and do their respective steps
};