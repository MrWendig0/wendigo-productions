#ifndef LIBRESCHOOL_H
#define LIBRESCHOOL_H

// This header contains general content used by both
// the server and its clients.

#define UINT8 unsigned char
#define INT8 signed char
#define INT16 signed short
#define UINT16 unsigned short

typedef struct {
  UINT8 questions:4;
        flags:4;
  char *strings[16][];
  UINT8	question_flags[16]; // 16 because that's the 4-bit int limit
  char *answer_strings[64][]; // max 64, bc 16 * 4 = 64
} LSC_ASSESSMENT

char lesson[65535];

typedef struct {
  UINT16 score;
  UINT8 assignments;
} LSC_STUDENT_DATA

#endif
