/*
GAME.H
This file defines SPECIFICALLY the game logic, completely separate from any libraries.
Drawing and handling of sprites happens in the main_[platform].c file.
*/

// these define the amount of frames in animations
#define PLR_IDLEFRAMES 7
#define PLR_WALKFRAMES 11

// according to Suckless, floats are overrated lmao
#define SUBPIXELUNIT_ACCURACY 32
// floats might be possible, just needs adaptation

// PRECALCULATIONS!!!
#define PRE_GRAVITY SUBPIXELUNIT_ACCURACY / 4 // precalculated gravity, also doubles as player movement speed

#define STATE_NORMAL 0
#define STATE_NOCONTROL 1

#define FLAG_GROUNDED 1
#define FLAG_CANTMOVE 1 << 1 // disables player control, if any
#define FLAG_DOPHYS 1 << 2 // universal; if set, object refuses to move at all
#define FLAG_DIDFOOTSTEP 1 << 3 // player flag; a simple debounce for footsteps

#define TFLAG_ACTIVE 1 << 7 // thing flag; is this thing NOT "in the room"?

// ROOM TRANSITION FLAGS
// oh wait, there aren't any

#define KEY_UP 1
#define KEY_DOWN 1 << 1
#define KEY_LEFT 1 << 2
#define KEY_RIGHT 1 << 3
#define KEY_JUMP 1 << 4
#define KEY_ATTACK 1 << 5

// baddies? the only baddies here are the demons in your head

// game states
#define GSTATE_MENU 0
#define GSTATE_PLAYING 1

#define GROUNDLEVEL 200

#define WIDTH 480
#define HEIGHT 270

void signalDraw(uint8_t signal); // interface with main for drawing specific things at specific times
void signalPlaySFX(uint8_t signal); // same stuff, but for sound effects

uint8_t GAME_STATE = 0;

typedef struct {
	int x, y, hsp, vsp;
	int x_sub, y_sub, hsp_sub, vsp_sub;
	uint8_t state, flags, keys, jumptimer;
	uint8_t animframe, animtimer;
} WTH_Player;

typedef struct {
	uint8_t menuselect;
} WTH_MainMenu;

typedef struct {
	int x, y;
} WTH_Camera;

typedef struct {
	int x, y;
	uint8_t objID; // used to actually defer what object this is, and uses the associated functions
	uint8_t flags;
	uint8_t vars[8];
} WTH_Interactible;

WTH_Player plr; // the one and only... uh... unstable furry man.
WTH_Camera cam; // and also the cameraman
WTH_Interactible interacts[256]; // all interactible objects that can be in a room
uint8_t interacts_count = 0; // convenient counter for all present interactibles

uint8_t level = 0; // all rooms, 256 is overkill but overkill is what's needed

WTH_MainMenu menu; // main menu

static inline void changeRoom(uint8_t input) {
	level = input;
	plr.x = 50;
	plr.y = GROUNDLEVEL;
	plr.hsp = plr.vsp = plr.hsp_sub = plr.vsp_sub = 0;
	LoadInRoom();
};

static inline uint8_t interact_step(WTH_Interactible *REF, uint8_t index) {
	if ((REF->flags & TFLAG_ACTIVE)) return;
	switch (index) {
		case 0: default:
			if (plr.x >= REF->x) {
				changeRoom(REF->vars[0]);
			};
			break;
		case 1:
			if (plr.x >= REF->x && plr.x < REF->x + 80) {
				printf("Player can enter door");
				if ((plr.keys & KEY_UP)) {
					changeRoom(REF->vars[0]);
				};
			};
			break;
	}
};

static inline void LoadInRoom() {
	for (uint8_t i = 0; i < interacts_count; i++) {
		if (!(interacts[i].flags & TFLAG_ACTIVE)) {
			interacts[i].flags = 0;
		};
	}
	interacts_count = 0;
	switch (level)
	{
		case 0:
			interacts[0].x = 164;
			interacts[0].objID = 1;
			interacts[0].vars[0] = 1;
			interacts_count++;
			break;
	}
};

void start() {
	plr.x = 50;
	plr.y = GROUNDLEVEL;
	plr.flags += FLAG_DOPHYS;
	cam.x = cam.y = 0;
	GAME_STATE = 1;
	LoadInRoom();
};

void step() {
	switch (GAME_STATE)
	{
	case 0: break;
	case 1:
		switch (plr.state)
		{
			case 0: 
				if (!(plr.flags & FLAG_CANTMOVE)) {
					if (plr.keys & KEY_LEFT) {
						printf("left");
						plr.hsp_sub -= 5;
						if (plr.animframe == 0 || plr.animframe == 6) {
							if ((plr.flags & FLAG_GROUNDED) && !(plr.flags & FLAG_DIDFOOTSTEP)) {
								signalPlaySFX(0);
								plr.flags += FLAG_DIDFOOTSTEP;
							};
						} else {
							if ((plr.flags & FLAG_DIDFOOTSTEP)) plr.flags -= FLAG_DIDFOOTSTEP;
						};
						if (plr.hsp > 0) plr.hsp = 0;
					};
					if (plr.keys & KEY_RIGHT) {
						printf("right");
						plr.hsp_sub += 5;
						if (plr.animframe == 0 || plr.animframe == 6) {
							if ((plr.flags & FLAG_GROUNDED) && !(plr.flags & FLAG_DIDFOOTSTEP)) {
								signalPlaySFX(0);
								plr.flags += FLAG_DIDFOOTSTEP;
								printf("footstep");
							};
						} else {
							if ((plr.flags & FLAG_DIDFOOTSTEP)) {
								plr.flags -= FLAG_DIDFOOTSTEP;
							};
						};
						if (plr.hsp < 0) plr.hsp = 0;
					};
					if (plr.hsp > 5) plr.hsp = 5;
					if (plr.hsp < -5) plr.hsp = -5;
					if (plr.jumptimer == 0 && (plr.keys & KEY_JUMP) && (plr.flags & FLAG_GROUNDED)) {
						plr.vsp = -3;
						plr.vsp_sub = 0;
						plr.flags -= FLAG_GROUNDED;
						signalPlaySFX(1);
						plr.jumptimer = 90;
					};
					if (!(plr.keys & KEY_LEFT) && !(plr.keys & KEY_RIGHT) && (plr.flags & FLAG_GROUNDED)) {
						plr.hsp = 0;
					};
					if (plr.jumptimer > 0) plr.jumptimer--;
				};
				break;
		}
		
		if (plr.flags & FLAG_DOPHYS) {
			printf("dophys");
			if (!(plr.flags & FLAG_GROUNDED)) {
				printf(" gravity");
				plr.vsp_sub += PRE_GRAVITY;
			}
			if (plr.y > GROUNDLEVEL || plr.y + plr.vsp > GROUNDLEVEL) {
				printf(" groundlevel");
				if (!(plr.flags & FLAG_GROUNDED)) {
					plr.flags |= FLAG_GROUNDED;
				};
				plr.vsp = 0;
				plr.vsp_sub = 0;
				plr.y = GROUNDLEVEL;
			};
			
			if (plr.vsp_sub > SUBPIXELUNIT_ACCURACY) {
				int calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
				plr.vsp += calc;
				plr.vsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
			};
			if (plr.vsp_sub < 0) {
				int calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
				plr.vsp -= calc;
				plr.vsp_sub += SUBPIXELUNIT_ACCURACY * calc;
			};
			
			if (plr.hsp_sub > SUBPIXELUNIT_ACCURACY) {
				int calc = (int)(plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
				plr.hsp += calc;
				plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
			};
			if (plr.hsp_sub < 0) {
				int calc = (int)(plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
				plr.hsp += calc;
				plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc; // HOW does this make ANY logical sense to work properly
			};
			plr.x += plr.hsp;
			plr.y += plr.vsp;
		};
		
		printf("%i", plr.animframe);
		plr.animtimer++;
		if (plr.animtimer >= 3) {
			plr.animframe++;
			plr.animtimer -= 3;
		};
		if (plr.animframe > PLR_WALKFRAMES) plr.animframe = 0;
		
		if (plr.x > cam.x + (WIDTH - 32)) {
			cam.x += plr.x - (cam.x + (WIDTH - 32));
		};
		if (plr.x < cam.x + 32) {
			cam.x -= (cam.x + 32) - plr.x;
		};
		
		for (uint8_t i = 0; i < interacts_count; i++) {
			interact_step(&interacts[i], interacts[i].objID);
		};
		plr.keys = 0;
		break;
	}
};