// Linux-native frontend for the game.
#include <stdio.h>
#define ALLOW_NONLINUX_COMP 1

#if !defined(linux) && !defined(ALLOW_NONLINUX_COMP)
	#error This ain't Linux. At least, I think not.
#endif

#define FAMILY_FRIENDLY 0

#ifdef FAMILY_FRIENDLY 1
	#define WINTITLE "Wake to Chaos"
#else
	#define WINTITLE "Wake to Hell"
#endif

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!

void signalPlaySFX(uint8_t signal) {};

int main() {
	uint8_t running = 1;
	start();
	while (running) {
		step();
	}
	
	return 0;
};