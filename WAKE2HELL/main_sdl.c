// SDL2 frontend for the game
#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#define FAMILY_FRIENDLY 0

#ifdef FAMILY_FRIENDLY 1
	#define WINTITLE "Wake to Chaos"
#else
	#define WINTITLE "Wake to Hell"
#endif

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!
Mix_Music *music;

Mix_Chunk *sfx[4];

SDL_Surface *winsurf;
SDL_Texture *wintext;

SDL_Surface *sprites[2];
SDL_Surface *plrsprites[2];

SDL_Rect dsp_rect; // this is used for the sprite's boundaries
SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position

uint8_t *keystates;

void drawSprite(SDL_Surface *sprite, int x, int y) {
	if (x <= -sprite->w || x > sprite->w + WIDTH) return;
	if (y <= -sprite->h || y > sprite->h + HEIGHT) return;
	if (x == y) dspdest_rect.x = dspdest_rect.y = x;
	else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.x = 0;
	dsp_rect.w = sprite->w;
	dsp_rect.h = sprite->h;
	SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
};

void drawRepeatingSprite(SDL_Surface *sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int16_t _x;
	if (x == y) dspdest_rect.x = dspdest_rect.y = _x = x;
	else {
		dspdest_rect.x = _x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.x = 0;
	dsp_rect.w = sprite->w;
	dsp_rect.h = sprite->h;
	printf("\n%i\n", WIDTH);
	for (; _x < WIDTH + sprite->w; _x += sprite->w) {
		if (_x < 0) {
			dspdest_rect.x = _x;
			continue;
		};
		SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
		printf("\n%i", dspdest_rect.x + sprite->w);
		printf("\n%i", _x + sprite->w);
		printf("%i\n", (dspdest_rect.x < WIDTH));
		dspdest_rect.x = _x;
	}
};


void drawSpriteSheeted(SDL_Surface *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist) { // supports only purely horizontal sheets
	if (x <= -x_sprdist || x > x_sprdist + WIDTH) return;
	if (y <= -y_sprdist || y > y_sprdist + HEIGHT) return;
	if (x == y) dspdest_rect.x = dspdest_rect.y = x;
	else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.x = x_sprdist * frame;
	dsp_rect.w = x_sprdist;
	dsp_rect.h = y_sprdist;
	SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
};

void signalPlaySFX(uint8_t signal) {
	Mix_PlayChannel(-1, sfx[signal], 0);
};

void draw() {
	switch (GAME_STATE)
	{
		case 1:
		
			switch (level)
			{
				case 0: // TEST
					SDL_FillRect(winsurf, NULL, 400);
					drawRepeatingSprite(sprites[0], 0 - cam.x, 0 - cam.y);
					break;
				case 1: // House
					SDL_FillRect(winsurf, NULL, 0x34373b); // convenient you can use hex codes for this
					break;
			};
			
			for (uint8_t i = 0; i < interacts_count; i++) {
				drawSprite(plrsprites[1], interacts[i].x - cam.x, interacts[i].y - cam.y);
			};
			
			drawSpriteSheeted(plr.hsp == 0 ? plrsprites[0] : plrsprites[1], (plr.x - 50) - cam.x, (plr.y - 50) - cam.y, plr.animframe, 100, 100);
			break;
	};
};

void keys() {
	keystates = SDL_GetKeyboardState(NULL);
	if (keystates[SDL_SCANCODE_LEFT]) plr.keys += KEY_LEFT;
	if (keystates[SDL_SCANCODE_RIGHT]) plr.keys += KEY_RIGHT;
	if (keystates[SDL_SCANCODE_UP]) plr.keys += KEY_UP;
	if (keystates[SDL_SCANCODE_Z]) plr.keys += KEY_JUMP;
};

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING | MIX_INIT_MOD) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0) printf("Audio cannot be initialized! Check your speakers...\n");
	
	SDL_Window *win = SDL_CreateWindow(WINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	SDL_Renderer *render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	winsurf = SDL_GetWindowSurface(win);
	
	music = Mix_LoadMUS("WRH.mp3");
	if (music) {
		Mix_PlayMusic(music, -1);
	} else printf("Can't load music file!\n");
	
	sfx[0] = Mix_LoadWAV("sfx_step.wav");
	sfx[1] = Mix_LoadWAV("sfx_jump.wav");
	sfx[2] = Mix_LoadWAV("sfx_slam.wav");
	
	plrsprites[0] = IMG_Load("sprites/plr_idle.png");
	plrsprites[1] = IMG_Load("sprites/plr_walk.png");
	sprites[0] = IMG_Load("sprites/bg.png");
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		keys();
		step();
		draw();
		
		wintext = SDL_CreateTextureFromSurface(render, winsurf);
		SDL_RenderCopy(render, wintext, NULL, NULL);
		//SDL_UpdateWindowSurface(win);
		SDL_RenderPresent(render);
		SDL_DestroyTexture(wintext);
		SDL_Delay(16.667f);
	}
	
	Mix_FreeMusic(music);
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};