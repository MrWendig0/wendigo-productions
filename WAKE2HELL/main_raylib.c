// Experimental Raylib frontend for the game

#include <stdio.h>

#include <raylib.h>

#define FAMILY_FRIENDLY 0

#ifdef FAMILY_FRIENDLY 1
	#define WINTITLE "Wake to Chaos"
#else
	#define WINTITLE "Wake to Hell"
#endif

#define BGBLUE (Color){0x01, 0x90, 255}

#include "game.h" // keep in mind, MAIN can still access variables from the game!
Music music;

Sound sfx[4];

Texture2D sprites[3];
Texture2D plrsprites[3];

uint8_t *keystates;
/*
void drawRepeatingSprite(SDL_Surface *sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.x = dsp_rect.y = 0;
	dsp_rect.w = sprite->w;
	dsp_rect.h = sprite->h;
	while (dspdest_rect.x < WIDTH) {
		SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
		dspdest_rect.x += WIDTH;
		SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
	};
};

void drawSpriteSheeted(SDL_Surface *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist) { // supports only purely horizontal sheets
	if (x <= -x_sprdist || x > x_sprdist + WIDTH) return;
	if (y <= -y_sprdist || y > y_sprdist + HEIGHT) return;
	if (x == y) dspdest_rect.x = dspdest_rect.y = x;
	else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	dsp_rect.x = x_sprdist * frame;
	dsp_rect.y = 0;
	dsp_rect.w = x_sprdist;
	dsp_rect.h = y_sprdist;
	SDL_BlitSurface(sprite, &dsp_rect, winsurf, &dspdest_rect);
};
*/

void signalPlaySFX(uint8_t signal) {
	PlaySound(sfx[signal]);
};
void signalDraw(uint8_t index) { return; };

void draw() {
	switch (GAME_STATE)
	{
		case 0:
			ClearBackground(BGBLUE);
			DrawTexture(sprites[2], 0, 0, WHITE);
			
			break;
		case 1:
			ClearBackground(WHITE);
			//drawBackground(sprites[0], 0 - cam.x, 0 - cam.y);
			
			//drawSpriteSheeted(plrsprites[0], (plr.x - 75) - cam.x, (plr.y - 100) - cam.y, plr.animframe, 150, 150);
			break;
	};
};

void keys() {
	return;
};

int main() {
	InitWindow(WIDTH, HEIGHT, WINTITLE);
	InitAudioDevice();
	
	SetTargetFPS(60);

	music = LoadMusicStream("WTH.mp3");
	PlayMusicStream(music);
	
	sfx[0] = LoadSound("sfx_step.wav");
	sfx[1] = LoadSound("sfx_jump.wav");
	sfx[2] = LoadSound("sfx_slam.wav");
	
	plrsprites[0] = LoadTexture("sprites/plr_idle.png");	
	sprites[0] = LoadTexture("sprites/bg.png");
	sprites[1] = LoadTexture("sprites/PLAY.png");
	sprites[2] = LoadTexture("sprites/MENU.png");
	
	start();
	while (!WindowShouldClose()) {
		keys();
		step();
		BeginDrawing();
		draw();
		EndDrawing();
	}
	
	//for (int i = 0; i < 1; i++) {
	//	Mix_FreeMusic(music[i]);
	//}
	
	return 0;
};