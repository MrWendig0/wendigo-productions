// why the hell would you play the game like this?
// keep in mind, the MAIN file handles ALL drawing, sounds, and inputs... there's nothing here.
// whatever you can just put your own stuff here idk
#include <stdio.h>

#if defined(WIN32) || defined(_WIN32) || defined(_WIN32_)
	#include <windows.h>
	#define DELAY(IN) (Sleep(IN))
#endif

#define FAMILY_FRIENDLY 0

#ifdef FAMILY_FRIENDLY 1
	#define WINTITLE "Wake to Chaos"
#else
	#define WINTITLE "Wake to Hell"
#endif

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!

void signalPlaySFX(uint8_t signal) {};

int main() {
	uint8_t running = 1;
	start();
	while (running) {
		step();
		DELAY(16.667f);
	}
	
	return 0;
};