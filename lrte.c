#include <stdio.h>
// we need this to open files
#include <SDL2/SDL.h>
// listen I have a good reason for this I swear;
// I don't wanna deal with the BS that programming
// separately for Linux and Windows gives so I am
// using SDL2 as a basic keystate handler,
// you can port this natively to either platform if
// you want; I'd appreciate it :D
#undef main

#define LRTE_MAX_BUFFER 2000000 // ~2 MB

FILE *file_in;
char file_content[LRTE_MAX_BUFFER];
char screenbuffer[50 * 50];

uint16_t yoffset;

void clearScreen() {
	for (char i = 0; i < 50; i++) {
		screenbuffer[i * 50] = '\n';
	};
};

int main(int argc, char *argv[]) {
	if (SDL_Init(SDL_INIT_EVENTS) != 0) {
		printf("Can't initalize SDL2! Exiting...");
		return 1;
	};
	
	if (argc < 2) {
		printf("Please provide a file as an argument.\nExample:lrte main.c");
		return 2;
	};

	file_in = fopen(argv[1], "rw");
	if (!file_in) {
		printf("Can't open file! Exiting...");
		return -2;
	};
	
	fread(&file_content, LRTE_MAX_BUFFER, 1, file_in);
	printf("LRTE - Less Retarded Text Editor");
	for (char i = 0; i < 50; i++) {
		printf("\n");
	};

	uint8_t running = 1;
	while (running) {
		printf("LRTE - Less Retarded Text Editor - Editing file ");
		printf(argv[2]);
		printf("\n");
		
		// translate file contents to screen buffer
		
		clearScreen();
		memcpy(&screenbuffer, &file_content + yoffset, 50 * 50);
		printf(screenbuffer);
	};
};
