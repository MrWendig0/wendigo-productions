// huge thanks to TinyCC's Windows API Hello World example program
// surprisingly, TinyCC is really optimized and the program's literally 5kb, even with these extra libraries

#include <windows.h>
#include <time.h>
#include <stdlib.h>

#define APPNAME "Snake"
#define MS 100
#define MAXSNAKEBLOCKS 100
// enter #define DEBUG to enable debug mode
#define DEBUG

#define PIXELSCALE 4

#define PRECALC_WINSIZE 50 << PIXELSCALE

char szAppName[] = APPNAME; // The name of this application
char szTitle[]   = APPNAME; // The title bar text

int snake_hdir = 0;
int snake_vdir = 0;
int snake_blocks[MAXSNAKEBLOCKS][2];
int snake_length = 2;
int food_x = 1;
int food_y = 1;

void changeSnakeBlocks(int input_1, int input_2) {
	snake_blocks[input_1][0] = snake_blocks[input_2][0];
	snake_blocks[input_1][1] = snake_blocks[input_2][1];
}

void CenterWindow(HWND hWnd);

void MoveSnake() {
	if (snake_hdir + snake_vdir == 0) return;
	// move the closest block to the head's position...
	changeSnakeBlocks(1, 0);
	// ...THEN move the head
	snake_blocks[0][0] += snake_hdir;
	snake_blocks[0][1] += snake_vdir;
	for (int i = snake_length; i > 1; i--) {
		if (snake_blocks[i - 1][0] == -1) continue; // no block here? skip to the next one
		changeSnakeBlocks(i, i -1);
	}
};

void RegenFood() {
	food_x = rand() % 48;
	food_y = rand() % 48;
	return;
};

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message) {

        // ----------------------- first and last
        case WM_CREATE:
			for (int i = 0; i < MAXSNAKEBLOCKS - 1; i++) {
				snake_blocks[i][0] = -1;
			};
			snake_blocks[0][0] = snake_blocks[0][1] = 24;
			snake_blocks[1][0] = 24;
			snake_blocks[1][1] = 25;
			RegenFood();
            SetTimer(hwnd, 1, MS, NULL); // "initialize" the "step"
            break;

        case WM_TIMER: // basically a step event
			MoveSnake();
			if (snake_blocks[0][0] == food_x && snake_blocks[0][1] == food_y) {
				snake_length++;
				changeSnakeBlocks(snake_length, snake_length - 1);
				RegenFood();
			}
			for (int i = MAXSNAKEBLOCKS; i > 1; i--) {
				if (snake_blocks[0][0] == snake_blocks[i][0] && snake_blocks[0][1] == snake_blocks[i][1]) {
					for (int i = 0; i < MAXSNAKEBLOCKS - 1; i++) {
						snake_blocks[i][0] = -1;
					};
					snake_length = 2;
					snake_blocks[0][0] = snake_blocks[0][1] = snake_blocks[1][0] = 24;
					snake_blocks[1][1] = 25;
					snake_hdir = snake_vdir = 0;
					RegenFood();
				};
			};
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        case WM_KEYDOWN: // input handling
			switch (wParam)
			{
				case 0x1B: // VK_ESCAPE
					DestroyWindow(hwnd);
					break;
				case 0x25: //VK_LEFTARROW
					snake_hdir = -1;
					snake_vdir = 0;
					break;
				case 0x27: //VK_RIGHTARROW
					snake_hdir = 1;
					snake_vdir = 0;
					break;
				case 0x26: //VK_UPARROW
					snake_vdir = -1;
					snake_hdir = 0;
					break;
				case 0x28: //VK_DOWNARROW
					snake_vdir = 1;
					snake_hdir = 0;
					break;
			}
            break;

        // this version of Snake is on a 50x50 playing field, upscaled by 8.
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC         hdc;
            hdc = BeginPaint(hwnd, &ps);
            SetBkMode(hdc, TRANSPARENT);
			
			HBRUSH GreenBrush = CreateSolidBrush(RGB(0, 255, 0));
			HBRUSH DGreenBrush = CreateSolidBrush(RGB(0, 100, 0));
			HBRUSH BlueBrush = CreateSolidBrush(RGB(0, 0, 255));
			
			SelectObject(hdc, GetStockObject(BLACK_BRUSH)); // change brush to black
			
			Rectangle(hdc, -1, -1, 800, 800);
			#ifdef DEBUG
			//SetTextColor(hdc, RGB(255, 255, 255));
			//DrawText(hdc, "Debug mode enabled!", -1, %rc, DT_SINGLELINE | | DT_CENTER | DT_TOP);
			#endif
			
			for (int i = 0; i < snake_length + 1; i++) {
				if (snake_blocks[i][0] == -1) continue;
				if (i == 0) SelectObject(hdc, GreenBrush); // color head bright green
				else SelectObject(hdc, DGreenBrush); // color body dark green
				Rectangle(hdc, snake_blocks[i][0] << PIXELSCALE, snake_blocks[i][1] << PIXELSCALE, snake_blocks[i][0] + 1 << PIXELSCALE, snake_blocks[i][1] + 1 << PIXELSCALE);
			}
			
			SelectObject(hdc, BlueBrush);
			Rectangle(hdc, food_x << PIXELSCALE, food_y << PIXELSCALE, food_x + 1 << PIXELSCALE, food_y + 1 << PIXELSCALE);
			
            EndPaint(hwnd, &ps);
            break;
        }

        // ----------------------- let windows do all other stuff
        default:
            return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

int APIENTRY WinMain(
        HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpCmdLine,
        int nCmdShow
        )
{
    MSG msg;
    WNDCLASS wc;
    HWND hwnd;
	
	srand(time(0) + 1);

    // Fill in window class structure with parameters that describe
    // the main window.

    ZeroMemory(&wc, sizeof wc);
    wc.hInstance     = hInstance;
    wc.lpszClassName = szAppName;
    wc.lpfnWndProc   = (WNDPROC)WndProc;
    wc.style         = CS_DBLCLKS|CS_VREDRAW|CS_HREDRAW;
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);

    if (FALSE == RegisterClass(&wc))
        return 0;

    // create the browser
    hwnd = CreateWindow(
        szAppName,
        szTitle,
        WS_OVERLAPPEDWINDOW|WS_VISIBLE,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        PRECALC_WINSIZE,//CW_USEDEFAULT,
        PRECALC_WINSIZE,//CW_USEDEFAULT,
        0,
        0,
        hInstance,
        0);

    if (NULL == hwnd)
        return 0;

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0) > 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    
	CenterWindow(hwnd);

    return msg.wParam;
}

void CenterWindow(HWND hwnd_self)
{
    HWND hwnd_parent;
    RECT rw_self, rc_parent, rw_parent;

    hwnd_parent = GetParent(hwnd_self);
    if (NULL == hwnd_parent)
        hwnd_parent = GetDesktopWindow();

    GetWindowRect(hwnd_parent, &rw_parent);
    GetClientRect(hwnd_parent, &rc_parent);
    GetWindowRect(hwnd_self, &rw_self);

    SetWindowPos(
        hwnd_self, NULL,
        rw_parent.left + (rc_parent.right + rw_self.left - rw_self.right) >> 1, //x pos
		rw_parent.top + (rc_parent.bottom + rw_self.top - rw_self.bottom) >> 1, //y pos
		0, 0,
        SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE
        );
}

//+---------------------------------------------------------------------------
