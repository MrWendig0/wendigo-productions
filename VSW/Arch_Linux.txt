Arch Linux is a do-it-yourself Linux distribution; it doesn't provide much by itself, leaving you with a terminal, but you can practically do ANYTHING with it once you get the hang of it. It comes with SystemD, a frowned upon init system. If you're one of these SystemD haters, but still like Arch, Artix may be a decent choice instead.

Since installation seems to be a daunting task, let me break it down;
This uses the Arch Linux installation guide, dumbed down to try removing a little fluff.
For the keymap; `{root}` is the root directory you'll install to, e.g. `/` or `/mnt/`, `{stuff}` is just literally anything. Placeholder numbers may be entered as X.

- Get the installation image for your system and put it on a CD or USB, then boot it
- Optionally, check your keyboard layout, console font, and time/date
- Connect to the Internet via `ip link`
- Make a partition from free space or by slicing off of an existing partition; if you know how to use fdisk or parted, you're golden
- Format the new partitions, mount them as their respective file systems (e.g. one partition for Linux like me = mount as /)
- `pacstrap -K {root}` some packages: `base`, `linux` or `linux-libre`, and `linux-firmware` at bare minimum for a functional system
- Use `fstab` to change how your Arch partition is mounted (e.g. for one-partition installations, make sure the Arch part. has <dir> as /)
- Use `arch-chroot` or `chroot` to change where your root is. You may still want to even on one-partition installations (e.g. `arch-chroot /dev/sdXX {root}`)
- Optionally, change your hostname by editing `/etc/hostname`
- Finally, change your password with `passwd {stuff}` and `reboot` your system. 

I MUST WARN YOU; doing the installation like this is very hazardous if you aren't careful. Get a /dev/sd wrong, you may wipe an existing OS partition and lose plenty of data. Take preventative measures and transport your data onto a SD card or USB stick, or put it on a cloud like Disroot or Google Drive. This applies to ALL installations of ANY system that gives you the liberty to choose what partition you want to put your system on.
