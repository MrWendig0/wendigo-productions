CC0, or the Creative Commons 0 license, is a license made by the Creative Commons that fully submits a given work to the public domain, minus any IPs or trademarks.

For licensing code to the public domain, the Unlicense, BSD0, or MIT No Attribution would be better.