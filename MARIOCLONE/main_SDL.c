/* MAIN_SDL
SDL frontend for the game. Requires SDL2, obviously.
*/

#define WINDOWS 1 //uses Windows by default. Some anarchist folk call this "Win$hit", but I simply don't even care much anymmore
//can be changed to LINUX if you're compiling for Linux SDL2.

#include <stdio.h>
#include <SDL2/SDL.h>
#ifdef WINDOWS
	#include <windows.h>
	#define GLOBAL_SLEEP(ms) Sleep(ms)
#else
	#error WINDOWS or LINUX must be defined in main_SDL.c!
#endif
#include "game.h"

#define INPUT_JUMP 1
#define INPUT_RUN 2
#define INPUT_UP 4
#define INPUT_DN 8
#define INPUT_LF 16
#define INPUT_RT 32

#undef main // TinyC doesn't like SDL's main

#define FPS 60
#define MSPF 1000 / FPS

void main() { // thank Anarch for a bit of this :D
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return; // initalize SDL
	
	SDL_Window *window = SDL_CreateWindow("Mr. Wendigo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 480, 270, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	//SDL_SetWindowSize(window, 960, 540);
	
	SDL_Renderer *render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	
	SDL_Surface *wsurf;
	wsurf = SDL_GetWindowSurface(window);
	
	uint8_t running = 1;
	SDL_Event event;
	

	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		step();
		draw(wsurf);

		SDL_UpdateWindowSurface(window);
		Sleep(16.667f);
	}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(window);
	SDL_Quit();
	
	return;
}