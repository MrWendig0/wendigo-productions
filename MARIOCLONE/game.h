#include <stdio.h>
#include <SDL2/SDL.h>

#define INPUT_JUMP 1
#define INPUT_RUN 2
#define INPUT_UP 4
#define INPUT_DN 8
#define INPUT_LF 16
#define INPUT_RT 32

#define PFLAG_GROUNDED 1
#define PFLAG_CANMOVE 2

typedef struct GPlayer {
	int x, y;
	float hsp, vsp;
	uint8_t pflags;
} GPlayer;
GPlayer plr;

//typedef struct GEnemy {
	
//};

SDL_Surface *plrsprite;
SDL_Rect rect = {0, 0, 480, 270};

void init() {
	plrsprite = SDL_LoadBMP("spr/wendigo/small.bmp");
};

void step() {
	if (!plr.pflags & PFLAG_GROUNDED) plr.vsp += 0.2;
	if (plr.y > 200) plr.y = 200; plr.pflags += PFLAG_GROUNDED;
	plr.y += plr.vsp;
};

void draw(SDL_Surface *surf) {
	SDL_BlitSurface(plrsprite, &rect, surf, &rect);
};