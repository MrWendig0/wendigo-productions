# Wendigo's Stuff

A repository of little projects I've made.
Stuff's under varying licenses; plenty are (and should be) CC0 unless stated otherwise, such as some parts of WAKE2HELL and the assets for PIZZATOWER.

By the way, I'm buddy-buddies with blitzdoughnuts.

**WAKE2HELL** is an old version of Wake to Hell, this time containing proprietary assets.\
**PIZZATOWER** is a "recreation" of Pizza Tower: Sage 2019 Demo in SDL2 and C, with some Pizza Tower assets.\
**MARIOCLONE** is a Mario-esque game I wanted to make due to there being no CC0 equivalent (Secret Chronicles and SuperTux are GPL'd) and was my first step into using SDL2 and experimenting with Drummyfish's philosophy of software design... which is *questionable* subjectively.\
**WINPIPE** is a little "backend" or template I made to assist with making games or software using the Windows API. It's barely at all done, I advise not even trying to use it. Besides, I was having plans to make it SDL2 compatible, anyway.\
**snakeWINDOWS.c** is a one-file simple Snake game made with just the Windows API. It should be pretty easy to port over to other platforms or frontends due to the trivial code, but be warned that the game code is scattered and not COMPLETELY separated.\
**SAMPLES** is a collection of synthesized samples by yours truly; modulated via several distortions and the likes, sometimes drawn by hand manually, all with Audacity and/or OpenMPT. **NOTE:** The MPMT folder has instruments that, while they have libre samples, may not have libre instrument mapping.\
**VSW** is a wiki inspired by the LRS Wiki that tries grounding itself more into reality. As in, current reality, spoiled by bad actors abusing capitalism to gain monopolies over our daily needs.